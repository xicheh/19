﻿// test1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;
class Animal
{
public:
	string voice;
};
class Dog : public Animal 
{};
class Cat : public Animal
{};
class Bird : public Animal
{};

int main()
{
	Dog dg;
	string& pa = dg.voice;
	pa = "Goolf!";
	cout << pa<<'\t'<<endl;
	Cat ct;
	string pb = ct.voice;
	pb = "Maayyy!";
	cout << pb << '\t'<<endl;
	Bird br;
	string& pc = br.voice;
	pc = "Chick Chiriick!";
	cout << pc << '\t' <<endl;
	return 0;

}


